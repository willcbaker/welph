#!/usr/bin/python

import string
import os
import glob

URL_WRAPPED = 'https://gitlab.com/willcbaker/welph/raw/master/'
URL_UNWRAPPED = 'https://gitlab.com/willcbaker/welph/raw/finanna/'
WRAPPED = 'imgs/wrap/{}.png'

FILE_GIFTS = 'gifts.csv'
HEADER_GIFTS = 'unwrapped,wrapped,text\n'

DIR_GIFTS = 'imgs/gifts'
CONFIG_GIFTS = os.path.join(DIR_GIFTS, 'config')

def generate_gifts_csv(filename, unwrapped, wrapped):
	with open(filename, 'w') as output_file:
		output_file.write(HEADER_GIFTS)

		for gift_unwrapped, gift_wrapped in zip(unwrapped, wrapped):
			text = gift_unwrapped[gift_unwrapped.rfind('/')+1:gift_unwrapped.rfind('.')]
			card_entry = '{},{},{}\n'.format(URL_UNWRAPPED+gift_unwrapped, URL_WRAPPED+gift_wrapped,text)
			output_file.write(card_entry)

def get_wrapped(length):
	gift_files = glob.glob(WRAPPED.format('*'))
	return [gift_files[gift_index%len(gift_files)] for gift_index in range(length)]

def get_unwrapped():
	config_gifts = open(CONFIG_GIFTS).read()
	if config_gifts.startswith('./'):
		config_gifts = os.path.join(os.path.dirname(CONFIG_GIFTS), config_gifts.strip('./'))
	unwrapped_images = glob.glob(config_gifts)
	return unwrapped_images

unwrapped = get_unwrapped()
wrapped = get_wrapped(len(unwrapped))

print 'We have {} players.'.format(len(players))
print 'We have {} wrapped gifts.'.format(len(wrapped))
print 'We have {} unwrapped gifts.'.format(len(unwrapped))

generate_gifts_csv(FILE_GIFTS, unwrapped, wrapped)
