# Holiday Gift Exchange Tool

Generates csv files for custom decks to be used at
[PlayingCards.io](https://playingcards.io/)

1. Add gifts to `imgs/gifts` directory. The name will be added to the cards. Example:
```sh
$ ls imgs/gifts
My\ Gift.png
Second\ Gift.png
...
```

1. Upload gift images to web host (files must be webhosted, can use git repository). E.g.:
```sh
git add -f imgs/gifts/*.png
git commit -m "add gifts" 
git push
```

1. Modify the webhost URL and run `./generate.py`

1. Upload resulting `gifts.csv` file to playingcards.io game.

1. (Optional) Use `template.pcio` as template game, if desired.\
`Edit Table -> Room Options -> Import From File`

![Example game board from template.pcio](/imgs/example.png "Example Game Board")